# Vagrant local development setup

The tl;dr version (`host$` and `vagrant$` prefix indicate where you're running
the command that follows):

```
host$ vagrant up
host$ vagrant ssh
vagrant$ /vagrant/reunhangout/devserver.py --addrport 0.0.0.0:8000
```
